<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{

    public function register(Request $request)
    {
        $object = $request->validate(
            [
                'username' => 'required',
                'email' => 'required',
                'password' => 'required',
            ],
            [
                'username.required' => 'Anda harus mengisi username!',
                'email.required' => 'Anda harus mengisi email!',
                'password.required' => 'Anda harus mengisi password!',
            ]
        );

        $user = User::create([
            'username' => $object['username'],
            'email' => $object['email'],
            'password' => bcrypt($object['password']),
        ]);

        $token = $user->createToken('API Token')->plainTextToken;
        $code = 200;
        return response()->json([
            'status' => 'Success',
            'message' => 'Register berhasil',
            'data' => $token
        ], $code);
    }

    public function login(Request $request)
    {
        $object = $request->validate([
            'username' => 'required',
            'password' => 'required',
        ]);


        if (!Auth::attempt($object)) {
            $code = 401;
            return response()->json([
                'status' => 'Error',
                'message' => 'Username atau password salah'
            ], $code);
        }

        $token = auth()->user()->createToken('API Token')->plainTextToken;
        $code = 200;
        return response()->json([
            'status' => 'Success',
            'message' => 'Login berhasil',
            'data' => $token
        ], $code);
    }

    public function logout()
    {
        auth()->user()->tokens()->delete();

        $code = 200;
        return response()->json([
            'status' => 'Success',
            'message' => 'Logout berhasil'
        ], $code);
    }
}
